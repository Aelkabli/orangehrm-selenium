package ma.youcode.orangeHRM.pages.pim;

import ma.youcode.orangeHRM.utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeleteEmployeePage {
    private WebDriver driver;
    private Waits waits;


    private static By PIM = By.xpath("//span[text()='PIM']");
    private static By iconDelete = By.xpath("//i[@class='oxd-icon bi-trash']");
    private static By confirmDelete = By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--label-danger orangehrm-button-margin']");


    public DeleteEmployeePage(WebDriver driver) {
        this.driver = driver;
        this.waits = new Waits(driver, 10);
    }

    public void deleteEmployee(){
        waits.waitForToBeClickable(PIM);
        driver.findElement(PIM).click();
        waits.waitForToBeClickable(iconDelete);
        driver.findElement(iconDelete).click();
        waits.waitForToBeClickable(confirmDelete);
        driver.findElement(confirmDelete).click();

    }
}
