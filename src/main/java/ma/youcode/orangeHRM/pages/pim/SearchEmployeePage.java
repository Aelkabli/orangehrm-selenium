package ma.youcode.orangeHRM.pages.pim;

import ma.youcode.orangeHRM.utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class SearchEmployeePage {
    private WebDriver driver;
    private Waits waits;


    private static By PIM = By.xpath("//span[text()='PIM']");
    private static By byName = By.xpath("//div[@data-v-75e744cd]//input[@placeholder='Type for hints...']");
    private static final By searchByStatus = By.xpath("//div[@class='oxd-select-text oxd-select-text--active']");

    private static By searchButton = By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary orangehrm-left-space']");


    public SearchEmployeePage(WebDriver driver) {

        this.driver = driver;
        this.waits = new Waits(driver, 10);
    }

    public void searchByName(String name){
        waits.waitForToBeClickable(PIM);
        driver.findElement(PIM).click();
        waits.waitToBeDisplayed(byName);
        driver.findElement(byName).sendKeys(name);
        waits.waitForToBeClickable(searchButton);
        driver.findElement(searchButton).click();
    }


  public void searchByStatus(){
      waits.waitForToBeClickable(PIM);
      driver.findElement(PIM).click();
      waits.waitForToBeClickable(searchByStatus);
      driver.findElement(searchByStatus).click();
      WebElement freelanceOption = driver.findElement(By.xpath("//div[@class='oxd-select-text oxd-select-text--active']//div[text()='Freelance']"));
      freelanceOption.click();
      waits.waitForToBeClickable(searchButton);
      driver.findElement(searchButton).click();
  }


}
