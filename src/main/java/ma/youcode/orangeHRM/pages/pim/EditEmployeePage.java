package ma.youcode.orangeHRM.pages.pim;

import ma.youcode.orangeHRM.utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class EditEmployeePage {
    private WebDriver driver;
    private Waits waits;

    private static By PIM = By.xpath("//span[text()='PIM']");
    private static By iconEdit = By.xpath("//i[@class='oxd-icon bi-pencil-fill']");
    private static By ContactDetails = By.xpath("//a[text()='Contact Details']");

    private static By Street1  = By.xpath("//input[@id='street1']");
    private static By Street2 = By.xpath("//input[@id='street2']");
    private static By City  = By.xpath("//input[@id='city']");
    private static By StateProvince = By.xpath("//input[@id='state']");
    private static By ZipPostalCode = By.xpath("//input[@id='zip']");
    private static By Country = By.xpath("//select[@id='country']");
    private static By HomePhone  = By.xpath("//input[@id='homePhone']");
    private static By MobilePhone = By.xpath("//input[@id='mobilePhone']");
    private static By  WorkPhone= By.xpath("//input[@id='workPhone'] ");
    private static By WorkEmail = By.xpath("//input[@id='workEmail']");
    private static By OtherEmail = By.xpath("//input[@id='otherEmail']");
    private static By SaveButton = By.xpath("//button[@type='submit' and contains(@class,'btn-secondary')]");
    private static By BoutonAdd  = By.xpath("//div[contains(@class, 'card-header')]//button[contains(@class, 'btn-primary')]");

    public EditEmployeePage(WebDriver driver) {
        this.driver = driver;
        this.waits = new Waits(driver, 10);
    }

    public void navigateToPIM() {
        waits.waitToBeClickableAndReturnElement(PIM).click();
    }

    public void clickEditButton() {
        waits.waitToBeClickableAndReturnElement(iconEdit).click();
        waits.waitToBeClickableAndReturnElement(ContactDetails).click();
    }

    public void fillContactDetails(String street1Value, String street2Value, String cityValue, String stateValue, String zipValue, String countryValue) {
        waits.waitToBeVisibleAndReturnElement(Street1).sendKeys(street1Value);
        waits.waitToBeVisibleAndReturnElement(Street2).sendKeys(street2Value);
        waits.waitToBeVisibleAndReturnElement(City).sendKeys(cityValue);
        waits.waitToBeVisibleAndReturnElement(StateProvince).sendKeys(stateValue);
        waits.waitToBeVisibleAndReturnElement(ZipPostalCode).sendKeys(zipValue);
        Select countrySelect = new Select(waits.waitToBeVisibleAndReturnElement(Country));
        countrySelect.selectByVisibleText(countryValue);
    }

    public void fillTelephoneNumbers(String home, String mobile, String work) {
        waits.waitToBeVisibleAndReturnElement(HomePhone).sendKeys(home);
        waits.waitToBeVisibleAndReturnElement(MobilePhone).sendKeys(mobile);
        waits.waitToBeVisibleAndReturnElement(WorkPhone).sendKeys(work);
    }

    public void fillEmails(String workEmailValue, String otherEmailValue) {
        waits.waitToBeVisibleAndReturnElement(WorkEmail).sendKeys(workEmailValue);
        waits.waitToBeVisibleAndReturnElement(OtherEmail).sendKeys(otherEmailValue);
    }

    public void saveChanges() {
        waits.waitToBeClickableAndReturnElement(SaveButton).click();
    }


}
