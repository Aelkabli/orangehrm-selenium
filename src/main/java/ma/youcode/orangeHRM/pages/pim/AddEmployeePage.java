package ma.youcode.orangeHRM.pages.pim;

import ma.youcode.orangeHRM.utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class AddEmployeePage {
    private WebDriver driver;
    private Waits waits;


    private static By PIM = By.xpath("//span[text()='PIM']");
    private static By addEmployee = By.xpath("//a[text()='Add Employee']");
    private static By firstNameField = By.xpath("//input[@name='firstName']");
    private static By middleNameField = By.xpath("//input[@name='middleName']");
    private static By lastNameField = By.xpath("//input[@name='lastName']");
    private static By idEmployeeField = By.xpath("//input[@class='oxd-input oxd-input--active']");
    private static By imageButton = By.xpath("//button[@class='oxd-icon-button oxd-icon-button--solid-main employee-image-action']");
    private static By saveButton = By.xpath("//button[@type='submit']");


    public AddEmployeePage(WebDriver driver) {

        this.driver = driver;
        this.waits = new Waits(driver, 10);
    }
    public void navigateToAddEmployee() {
        waits.waitForToBeClickable(PIM);
        driver.findElement(PIM).click();
        waits.waitForToBeClickable(addEmployee);
        driver.findElement(addEmployee).click();
    }

    public void addEmployee(String firstName, String middleName, String lastName, String idEmployee){
        waits.waitToBeDisplayed(firstNameField);
        driver.findElement(firstNameField).sendKeys(firstName);
        waits.waitToBeDisplayed(middleNameField);
        driver.findElement(middleNameField).sendKeys(middleName);
        waits.waitToBeDisplayed(lastNameField);
        driver.findElement(lastNameField).sendKeys(lastName);
        waits.waitToBeDisplayed(idEmployeeField);
        driver.findElement(idEmployeeField).sendKeys(idEmployee);
        waits.waitForToBeClickable(saveButton);
        driver.findElement(saveButton).click();
    }
    public void uploadEmployeeImage(String imagePath) {
        driver.findElement(imageButton).click();


    }
}
