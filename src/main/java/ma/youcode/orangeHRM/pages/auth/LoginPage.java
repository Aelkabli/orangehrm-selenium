package ma.youcode.orangeHRM.pages.auth;

import ma.youcode.orangeHRM.utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private WebDriver driver;
    private Waits waits;


    private static final By usernameField = By.xpath("//input[@name='username']");
    private static final By passwordField = By.xpath("//input[@placeholder='Password']");
    private static final By loginButton = By.xpath("//button[@type='submit']");



    public LoginPage(WebDriver driver) {

        this.driver = driver;
        this.waits = new Waits(driver, 10);
    }

    public void loginValid(String username, String password) {
        waits.waitForToBeClickable(usernameField);
        driver.findElement(usernameField).sendKeys(username);
        waits.waitForToBeClickable(passwordField);
        driver.findElement(passwordField).sendKeys(password);
        waits.waitForToBeClickable(loginButton);
        driver.findElement(loginButton).click();
    }







}
