package ma.youcode.orangeHRM.pages.auth;

import ma.youcode.orangeHRM.utils.Waits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogoutPage {
    private WebDriver driver;
    private Waits waits;



    public LogoutPage(WebDriver driver) {

        this.driver = driver;
        this.waits = new Waits(driver, 10);
    }

    private static By btnUserDropdown = By.xpath("//span[@class='oxd-userdropdown-tab']");
    private static  By logoutButton = By.xpath("//a[@href='/web/index.php/auth/logout']");


    public void clickOnUserDropdownBtn(){
        waits.waitForToBeClickable(btnUserDropdown);
        driver.findElement(btnUserDropdown).click();
    }

    public void clickOnLogoutBtn(){
        waits.waitForToBeClickable(logoutButton);
        driver.findElement(logoutButton).click();
    }

}
