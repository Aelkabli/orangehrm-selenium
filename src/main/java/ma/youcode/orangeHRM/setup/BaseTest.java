package ma.youcode.orangeHRM.setup;


import io.github.bonigarcia.wdm.WebDriverManager;
import ma.youcode.orangeHRM.pages.auth.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


import java.lang.reflect.Method;
import java.time.Duration;

public class BaseTest {
    public WebDriver driver;
    public LoginPage loginPage;

    public String baseUrl = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";

    @BeforeMethod
    public void setUp(Method method){
        try {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            driver.get(baseUrl);
            loginPage = new LoginPage(driver);
            if (!method.getName().equals("loginTestWithInvalidCredential") && !method.getName().equals("emptyUsernameTest")) {
                loginPage.loginValid("Admin", "admin123");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }


}
