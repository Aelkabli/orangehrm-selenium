package ma.youcode.orangeHRM.tests.auth;


import ma.youcode.orangeHRM.pages.auth.LogoutPage;
import ma.youcode.orangeHRM.setup.BaseTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogoutTest extends BaseTest {
    private LogoutPage logoutPage;
    @BeforeMethod
    public void setup(){
        logoutPage = new LogoutPage(driver);
    }

    @Test
    public void Logout(){
        logoutPage.clickOnUserDropdownBtn();
        logoutPage.clickOnLogoutBtn();
    }
}
