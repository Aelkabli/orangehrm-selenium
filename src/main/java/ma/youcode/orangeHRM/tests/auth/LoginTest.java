package ma.youcode.orangeHRM.tests.auth;

import ma.youcode.orangeHRM.pages.auth.LoginPage;
import ma.youcode.orangeHRM.setup.BaseTest;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest  extends BaseTest {
    private LoginPage loginPage; 0664974681

    @Test(priority = 1)
    public void loginTestWithValidCredential(){
        loginPage= new LoginPage(driver);
        loginPage.loginValid("Admin", "admin123");
        String Title = driver.getTitle();
        Assert.assertEquals("OrangeHRM", Title);

    }

    @Test(priority = 2)
    public void loginTestWithInvalidCredential(){
        loginPage = new LoginPage(driver);
        loginPage.loginValid("Admin", "admin");
        String errorMessage = driver.findElement(By.xpath("//p[@class='oxd-text oxd-text--p oxd-alert-content-text']")).getText();
        Assert.assertTrue(errorMessage.contains("Invalid credentials"));
    }
    @Test(priority = 3)
    public void emptyUsernameTest(){
        loginPage = new LoginPage(driver);
        loginPage.loginValid("", "");
        String errorMessage = driver.findElement(By.xpath("//p[@class='oxd-text oxd-text--p oxd-alert-content-text']")).getText();
        Assert.assertTrue(errorMessage.contains("Username cannot be empty"));

    }



}
