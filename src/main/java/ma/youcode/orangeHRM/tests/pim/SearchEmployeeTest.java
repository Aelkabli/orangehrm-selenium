package ma.youcode.orangeHRM.tests.pim;

import ma.youcode.orangeHRM.pages.auth.LoginPage;
import ma.youcode.orangeHRM.pages.pim.AddEmployeePage;
import ma.youcode.orangeHRM.pages.pim.SearchEmployeePage;
import ma.youcode.orangeHRM.setup.BaseTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SearchEmployeeTest extends BaseTest {

    private SearchEmployeePage searchEmployeePage;
    @BeforeMethod
    public void setup(){
        searchEmployeePage= new SearchEmployeePage(driver);
    }

    @Test(priority = 1)
    public void searchByName(){
        searchEmployeePage.searchByName("Emily");
    }

    @Test(priority = 2)
    public void searchByStatusTest(){
        searchEmployeePage.searchByStatus();
    }


}
