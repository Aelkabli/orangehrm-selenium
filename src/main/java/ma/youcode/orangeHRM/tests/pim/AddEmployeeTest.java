package ma.youcode.orangeHRM.tests.pim;

import ma.youcode.orangeHRM.pages.pim.AddEmployeePage;
import ma.youcode.orangeHRM.pages.auth.LoginPage;
import ma.youcode.orangeHRM.setup.BaseTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddEmployeeTest  extends BaseTest {
    private AddEmployeePage addEmployeePage;
    @BeforeMethod
    public void setup(){
        addEmployeePage = new AddEmployeePage(driver);
        addEmployeePage.navigateToAddEmployee();
    }
    @Test
    public void addEmployee(){

        addEmployeePage.addEmployee("amina", "aa", "elkabli", "12");
    }
}
