package ma.youcode.orangeHRM.tests.pim;


import ma.youcode.orangeHRM.pages.pim.EditEmployeePage;
import ma.youcode.orangeHRM.setup.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EditEmployeeTest extends BaseTest {
    private EditEmployeePage editEmployeePage;
    @BeforeMethod
    public void setup(){
        editEmployeePage = new EditEmployeePage(driver);
        editEmployeePage.navigateToPIM();
        editEmployeePage.clickEditButton();
    }
    @Test
    public void editEmployeeTest(){
        String street1 = "123 Test St";
        String street2 = "Suite 100";
        String city = "TestCity";
        String state = "TestState";
        String zip = "12345";
        String country = "USA";
        String homePhone = "123-456-7890";
        String mobilePhone = "234-567-8901";
        String workPhone = "345-678-9012";
        String workEmail = "test@testdomain.com";
        String otherEmail = "other@testdomain.com";
        editEmployeePage.fillContactDetails(street1, street2, city, state, zip, country);
        editEmployeePage.fillTelephoneNumbers(homePhone, mobilePhone, workPhone);
        editEmployeePage.fillEmails(workEmail, otherEmail);
        editEmployeePage.saveChanges();

        String successMessage = driver.findElement(By.xpath("//div[contains(text(), 'Success')]")).getText();
        Assert.assertTrue(successMessage.contains("Success"), "Expected success message not displayed");


        Assert.assertEquals(driver.findElement(By.id("street1")).getAttribute("value"), street1, "Street 1 not updated correctly");

    }

    }

