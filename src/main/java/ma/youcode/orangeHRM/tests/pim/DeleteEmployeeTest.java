package ma.youcode.orangeHRM.tests.pim;

import ma.youcode.orangeHRM.pages.pim.DeleteEmployeePage;
import ma.youcode.orangeHRM.setup.BaseTest;
import org.testng.annotations.Test;

public class DeleteEmployeeTest extends BaseTest {
    private DeleteEmployeePage deleteEmployeePage;

    @Test
    public void deleteEmployee(){
        deleteEmployeePage = new DeleteEmployeePage(driver);
        deleteEmployeePage.deleteEmployee();
    }
}
